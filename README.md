# PyTorch_ObjectDetection

PyTorch is an open-source machine learning platform that is popular amongst the autonomous vehicle community for its ability to accurately identify objects. This allows PyTorch to detect objects in front of autonomous vehicles, giving the driver and/or vehicle time to make a decision. With one of PyTorch's key applications being image recognition, using it to detect objects on train tracks is the logical next step.

Rail is the most fuel-efficient way to move freight, taking cargo that would have taken over several hundred trucks to carry nearly 500 miles per gallon of fuel on a single train. Despite this, of the 1.6 million railcars within the U.S, over 900,000 spend nearly 23 hours idling in train yards before being placed on the right tracks. This is a huge problem, especially considering that moving cargo by rail reduces greenhouse gas emissions by up to 75%.

My goal is to build a real time object detection system to be used on locomotives in train yards to reduce the idling time of freight and reduce greenhouse gas emmissions.
